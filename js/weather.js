// Author : Jacek Jasiński
// Datetime : 29.11.2017
// Requirements : underscore.js & jquery & bootstrap 4
//
// Installation :
// Add this inside your body element if you have jquery and bootstrap 4 loaded already:
//
// <script src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
// <script src="js/weather.js"></script>
//


var body = $('body');
body.append('<div id="weather"></div>');
body.append("<style>#weather {  position: fixed;  width: 190px;  left: -0px;  top: 40%;   background: #009888;     padding: 10px;  border-top-right-radius: 10px;  border-bottom-right-radius: 10px; } #weather .city:not(:last-of-type) {margin-bottom:15px;} #weather .city {   width: 100%;    }  #weather .city small:first-of-type {  font-size:10px;width:100%;text-align: center; } #weather .city .name {line-height: 16px;font-size:14px;width:100%;text-align: center;font-weight: bold;} #weather .city a {  color: #fff;  font-size:16px } #weather .city strong {width:100%;text-align: center;font-size:14px;} #weather .city strong b {font-size:16px;margin-right:8px;color:gold;}</style>");

$(function () {

    function getThree() {
        var geo = {places: ['Lodz, Polska', 'Warszawa, Polska', 'Berlin, Niemcy', 'New york', 'London']};
        geo = _.shuffle(geo.places);
        var take = [];
        for (var index = 0; index < 3; index++) {
            take[index] = geo[index];
        }
        return take;
    }

    var cities = getThree();
    setInterval(function () {
        cities = getThree()
    }, 60000);

    var watcher = function () {
        var html = '';
        var cnt = 0;
        _.each(cities, function (city, index) {
            $.getJSON("https://query.yahooapis.com/v1/public/yql?q=select * from weather.forecast where woeid in (select woeid from geo.places(1) where text='" + city + "') and u='C'&format=json", function (data) {
                html += '<div class="city">' +
                    '<a class="d-flex flex-column" href="'+(data.query.results.channel.item.link).replace('http://us.rd.yahoo.com/dailynews/rss/weather/Country__Country/*', '')+'" target="_blank">' +
                    '<small>'+data.query.results.channel.item.condition.date+'</small>' +
                    '<span class="name">'+(data.query.results.channel.title).replace('Yahoo! Weather - ', '')+'</span>' +
                    '<strong><small><b>'+data.query.results.channel.item.condition.temp+'°C</b></small>'+data.query.results.channel.item.condition.text+'</strong></a></div>';
                cnt++;
                if(cnt === 3) {
                    $('#weather').html(html);
                }
            });

        });

        setTimeout(function () {
            clearInterval(watcher);
            watcher();
        },10000);
    };
    watcher();

});
